using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cashier : MonoBehaviour
{
    [SerializeField] private ScoreManager scoreManager;

    [SerializeField] private GameObject dialogWindow;
    [SerializeField] private GameObject foodContainer;
    [SerializeField] private StoragePanel storagePanel;

    private Client client;

    private string productsSpritesPath = "Sprites/Products/";
    private string iconRightPath = "Sprites/Checkmarks/icon_right";
    private string iconWrongPath = "Sprites/Checkmarks/icon_wrong";

    private List<string> orderedProducts;
    public List<string> collectedOrderProducts = new List<string>();

    private int rightCollectedOrderProductsCounter = 0;
    public int selectedProductsAmount = 0;

    private string OnProductOrderCollectedEventName = "OnProductOrderCollected";

    private void Start()
    {
        storagePanel.OnFoodButtonClicked += OnUIItemSelected;
    }

    private void OnEnable()
    {
        EventManager.StartListening(OnProductOrderCollectedEventName, OnProductOrderCollected);
    }

    private void OnDisable()
    {
        EventManager.StopListening(OnProductOrderCollectedEventName, OnProductOrderCollected);
    }


    public void ApplyClient(Client client)
    {
        this.client = client;
    }

    public void RefreshSavedData()
    {
        for (int i = 0; i < foodContainer.transform.childCount; i++)
        {
            foodContainer.transform.GetChild(i).GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            foodContainer.transform.GetChild(i).GetComponent<Image>().sprite = null;
            foodContainer.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
        }

        orderedProducts?.Clear();
        collectedOrderProducts?.Clear();

        rightCollectedOrderProductsCounter = 0;
        selectedProductsAmount = 0;
    }

    public void AddProductToCollectedOrder(string product)
    {
        collectedOrderProducts.Add(product);
    }

    public void RemoveProductFromCollectedOrder(string product)
    {
        collectedOrderProducts.Remove(product);
    }

    public void ChangeProductsAmount(int value)
    {
        selectedProductsAmount = Mathf.Clamp(selectedProductsAmount + value, 0, client.GetOrderedProductsAmount());

        if (selectedProductsAmount == client.GetOrderedProductsAmount())
        {
            storagePanel.DisableAllClickedFoodButtons(true);
            storagePanel.SetButtonSellActive(true);
        }
        else
        {
            storagePanel.DisableAllClickedFoodButtons(false);
            storagePanel.SetButtonSellActive(false);
        }
    }

    // Checks if all products collected by the seller match the order
    private bool IsTheOrderAssembledCorrectly()
    {
        return rightCollectedOrderProductsCounter == client.GetOrderedProductsAmount();
    }

    // Check if the product from the order collected by the seller is included in the buyer's order
    private void CheckProduct(int productId)
    {
        orderedProducts = client.orderedProducts;

        if (orderedProducts.Contains(collectedOrderProducts[productId]))
        {
            MarkCollectedOrderProductAsRight(productId, true);
            rightCollectedOrderProductsCounter++;
        }
        else
        {
            MarkCollectedOrderProductAsRight(productId, false);
        }
    }

    // The product is marked as right or wrong depending on its conformity with the customer's order
    private void MarkCollectedOrderProductAsRight(int productId, bool right)
    {
        var product = foodContainer.transform.GetChild(productId);
        var productCheckmark = product.GetChild(0);
        var productImage = product.GetComponent<Image>();
        var productColor = productImage.color;
        productColor.a = 0.3f;
        productImage.color = productColor;

        var checkmarkIconPath = right ? iconRightPath : iconWrongPath;
        productCheckmark.gameObject.SetActive(true);
        productCheckmark.GetComponent<Image>().sprite = Resources.Load<Sprite>(checkmarkIconPath);
    }

    // Displays products collected by the seller
    private void OnProductOrderCollected()
    {
        SetVisibleDialogWindow(true);

        for (int productId = 0; productId < collectedOrderProducts.Count; productId++)
        {
            var spritePath = productsSpritesPath + collectedOrderProducts[productId];
            foodContainer.transform.GetChild(productId).GetComponent<Image>().sprite = Resources.Load<Sprite>(spritePath);
        }

        ApplyTimerToCheckTheCollectedOrder();
    }

    private void SetVisibleDialogWindow(bool value)
    {
        dialogWindow.SetActive(value);

        SoundManager.Instance.PlayDialogWindowAppearSound(value);
    }

    private void ApplyTimerToCheckTheCollectedOrder()
    {
        StartCoroutine(StartTimerToCheckTheCollectedOrder(1f));
    }

    private void ApplyTimerToCheckIfEachProductMatchesTheOrder()
    {
        StartCoroutine(StartTimerToCheckEachProductInOrder(0.5f));
    }

    private void OnPurchaseCompleted()
    {
        client.DisplayEmoji(IsTheOrderAssembledCorrectly());
        SetVisibleDialogWindow(false);

        scoreManager.UpdateScore(IsTheOrderAssembledCorrectly(), rightCollectedOrderProductsCounter);
    }

    IEnumerator StartTimerToCheckTheCollectedOrder(float timeInSec)
    {
        yield return new WaitForSeconds(timeInSec);
        ApplyTimerToCheckIfEachProductMatchesTheOrder();
    }

    // Timer with an interval of 0.5 seconds to check the compliance of each product desired by the client with the completed order
    IEnumerator StartTimerToCheckEachProductInOrder(float timeInSec)
    {
        int iterationCounter = 0;
        
        while (iterationCounter != collectedOrderProducts.Count)
        {
            CheckProduct(iterationCounter);
            iterationCounter++;
            yield return new WaitForSeconds(timeInSec);
        }

        StartCoroutine(StartTimerToCompletePurchase());
    }

    IEnumerator StartTimerToCompletePurchase()
    {
        yield return new WaitForSeconds(1f);

        OnPurchaseCompleted();
    }

    private bool OnUIItemSelected(FoodButton button)
    {
        if (button.IsSelected())
        {
            if (!IsCanSelectNewFood())
            {
                return false;
            }

            ChangeProductsAmount(1);
            AddProductToCollectedOrder(button.name);
        }
        else
        {
            ChangeProductsAmount(-1);
            RemoveProductFromCollectedOrder(button.name);
        }

        return true;
    }
    private bool IsCanSelectNewFood()
    {
        return selectedProductsAmount + 1 <= client.GetOrderedProductsAmount();
    }
}
