using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Client : MonoBehaviour
{
    [SerializeField] private GameObject foodContainer;
    [SerializeField] private GameObject dialogWindow;
    [SerializeField] private GameObject emojiView;

    public List<string> orderedProducts = new List<string>();

    private Vector3 startPosition = new Vector3(-600, 50, 0);
    private Vector3 cashierPosition = new Vector3(-225, -25, 0);

    private float movingDuration = 3f;
    private int bounceSpeed = 25;
    private int bounceRange = 5;

    private string productsSpritesPath = "Sprites/Products/";
    private string iconNegativeEmojiPath = "Sprites/Emoji/emoji_angry";
    private string iconPositiveEmojiPath = "Sprites/Emoji/emoji_like";

    private string OnClientEnteredEventName = "OnClientEntered";
    private string OnClientLeftEventName = "OnClientLeft";


    private void OnEnable()
    {
        EventManager.StartListening(OnClientEnteredEventName, OnClientEntered);
        EventManager.StartListening(OnClientLeftEventName, OnClientLeft);
    }

    private void OnDisable()
    {
        EventManager.StopListening(OnClientEnteredEventName, OnClientEntered);
        EventManager.StopListening(OnClientLeftEventName, OnClientLeft);
    }

    private void Start()
    {
        dialogWindow.SetActive(false);
        transform.localPosition = startPosition;

        MoveToCashier();
    }

    public int GetOrderedProductsAmount()
    {
        return orderedProducts.Count;
    }

    public void DisplayEmoji(bool orderIsCorrectly)
    {
        SetVisibleDialogIdeaWindow(true);
        foodContainer.SetActive(false);

        emojiView.SetActive(true);
        var iconEmojiPath = orderIsCorrectly ? iconPositiveEmojiPath : iconNegativeEmojiPath;
        emojiView.GetComponent<Image>().sprite = Resources.Load<Sprite>(iconEmojiPath);

        MoveOut();
    }

    private void MoveToCashier()
    {
        transform.localScale = new Vector3(1, 1, 1);
        StartCoroutine(AnimateMove(startPosition, cashierPosition, movingDuration, true));
    }

    private void MoveOut()
    {
        transform.localScale = new Vector3(-1, 1, 1);
        StartCoroutine(AnimateMove(cashierPosition, startPosition, movingDuration, false));
    }

    // Calculate 3 random non-repeating product indices and get products from the store based on them
    private void MakeAnOrder()
    {
        List<string> productsInStorage = FoodStorage.productsInStorage;
        // Choose how many products the client wants to buy
        var possibleProductsAmount = Random.Range(1, 4);

        for (int productId = 0; productId < possibleProductsAmount; productId++)
        {
            var randomProductIndex = Random.Range(0, productsInStorage.Count);
            orderedProducts.Add(productsInStorage[randomProductIndex]);
            productsInStorage.RemoveAt(randomProductIndex);
        }

        DisplayOrderedProducts();
    }

    private void SetVisibleDialogIdeaWindow(bool value)
    {
        dialogWindow.SetActive(value);

        SoundManager.Instance.PlayDialogWindowAppearSound(value);
    }

    private void DisplayOrderedProducts()  
    {
        SetVisibleDialogIdeaWindow(true);

        for (int productId = 0; productId < GetOrderedProductsAmount(); productId++)
        {
            var spritePath = productsSpritesPath + orderedProducts[productId];
            foodContainer.transform.GetChild(productId).GetComponent<Image>().sprite = Resources.Load<Sprite>(spritePath);
        }
    }

    private void OnClientEntered()
    {
        MakeAnOrder();

        // After 5 seconds show Storage Panel
        StartCoroutine(ApplyTimerForStoragePanelAppearing(5f));
    }

    private void OnClientLeft()
    {
        EventManager.TriggerEvent("OnGameCycleCompleted");

        Destroy(this.gameObject);
    }

    IEnumerator AnimateMove(Vector3 currentPos, Vector3 targetPos, float duration, bool toCashier)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey += Time.deltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            transform.localPosition = Vector3.Lerp(currentPos, targetPos, percent);
            transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Sin(Time.time * bounceSpeed) * bounceRange, transform.localPosition.z);

            yield return null;
        }

        var eventName = toCashier ? OnClientEnteredEventName : OnClientLeftEventName;
        EventManager.TriggerEvent(eventName);
    }

    IEnumerator ApplyTimerForStoragePanelAppearing(float timeInSec)
    {
        yield return new WaitForSeconds(timeInSec);

        SetVisibleDialogIdeaWindow(false);
        EventManager.TriggerEvent("OnStoragePanelAppearTimerCompleted");
    }
}
