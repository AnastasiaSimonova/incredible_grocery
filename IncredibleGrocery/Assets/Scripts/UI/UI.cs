using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private SettingsManager settingsManager;

    public void SettingsMenuSetVisible(bool value)
    {
        settingsManager.SetVisible(value);
        gameManager.PauseGame(value);
    }
}
