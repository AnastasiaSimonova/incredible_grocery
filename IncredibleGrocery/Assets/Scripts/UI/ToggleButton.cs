using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleButton : MonoBehaviour
{
    [SerializeField] private Text labelText;


    public void ToggleOnValueChange(bool setOn)
    {
        ApplyLabelText(setOn);
    }

    private void ApplyLabelText(bool setOn)
    {
        if (setOn)
        {
            labelText.text = "ON";
        }
        else
        {
            labelText.text = "OFF";
        }
    }
}
