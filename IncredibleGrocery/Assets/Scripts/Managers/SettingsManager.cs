using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    [SerializeField] private ToggleButton soundButton;
    [SerializeField] private ToggleButton musicButton;
    [SerializeField] private GameObject settingsPanel;
    [SerializeField] private GameObject background;

    //private Color backgroundColor;

    private bool isSound;
    private bool isMusic;

    private Color startColor = new Color(0.084f, 0.084f, 0.084f, 0f);
    private Color finalColor = new Color(0.084f, 0.084f, 0.084f, 0.84f);

    private Vector3 startPos = new Vector3(0, -610, 0);
    private Vector3 finalPos = new Vector3(0, 0, 0);
    private float animDuration = 0.3f;


    private void Start()
    {
        LoadSettings();

        soundButton.GetComponent<Toggle>().isOn = isSound;
        musicButton.GetComponent<Toggle>().isOn = isMusic;

        //backgroundColor = background.GetComponent<Image>().color;
    }

    private void LoadSettings()
    {
        isSound = SaveManager.IsSound() == 1 ? true : false;
        isMusic = SaveManager.IsMusic() == 1 ? true : false;
    }

    public void ChangeIsSound(bool isSound)
    {
        this.isSound = isSound;
    }

    public void ChangeIsMusic(bool isMusic)
    {
        this.isMusic = isMusic;
    }

    public void SaveSettings()
    {
        SaveManager.SaveIsSound(isSound == true ? 1 : 0);
        SaveManager.SaveIsMusic(isMusic == true ? 1 : 0);
    }

    public void SetVisible(bool visible)
    {
        if (visible)
        {
            gameObject.SetActive(visible);
            StartCoroutine(AnimateMove(settingsPanel, startPos, finalPos, animDuration, true));
            StartCoroutine(AnimateChangeColor(startColor, finalColor, animDuration));
        }
        else
        {
            StartCoroutine(AnimateMove(settingsPanel, finalPos, startPos, animDuration, false));
            StartCoroutine(AnimateChangeColor(finalColor, startColor, animDuration));
        }
    }

    IEnumerator AnimateMove(GameObject obj, Vector3 startPos, Vector3 targetPos, float duration, bool toGameScreen)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey += Time.unscaledDeltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            obj.transform.localPosition = Vector3.Lerp(startPos, targetPos, percent);

            yield return null;
        }
        if (!toGameScreen)
            gameObject.SetActive(false);
    }

    IEnumerator AnimateChangeColor(Color startColor, Color targetColor, float duration)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey += Time.unscaledDeltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            //backgroundColor = Color.Lerp(startColor, targetColor, percent);
            background.GetComponent<Image>().color = Color.Lerp(startColor, targetColor, percent);

            yield return null;
        }
    }
}
