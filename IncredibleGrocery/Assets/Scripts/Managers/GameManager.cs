using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject clientView;
    [SerializeField] private GameObject clientPrefab;

    [SerializeField] private Cashier cashier;

    private Client client;

    private string OnGameCycleCompletedEventName = "OnGameCycleCompleted";


    private void OnEnable()
    {
        EventManager.StartListening(OnGameCycleCompletedEventName, OnGameCycleCompleted);
    }

    private void OnDisable()
    {
        EventManager.StopListening(OnGameCycleCompletedEventName, OnGameCycleCompleted);
    }

    private void Start()
    {
        CreateNewClient();
        cashier.ApplyClient(client);
    }

    public void CreateNewGameData()
    {
        CreateNewClient();

        cashier.ApplyClient(client);
        cashier.RefreshSavedData();

        GameObject[] foodButtons = GameObject.FindGameObjectsWithTag("FoodButton");
        foreach (GameObject foodButton in foodButtons)
        {
            foodButton.GetComponent<FoodButton>().Reset();
        }

    }

    public void CreateNewClient()
    {
        var newClient = Instantiate(clientPrefab);
        newClient.transform.SetParent(clientView.transform);

        client = newClient.GetComponent<Client>();
    }

    public void PauseGame(bool value)
    {
        Time.timeScale = value ? 0 : 1;
    }

    IEnumerator StartTimerToCreateNewClient()
    {
        yield return new WaitForSeconds(1f);

        CreateNewGameData();
    }

    private void OnGameCycleCompleted()
    {
        StartCoroutine(StartTimerToCreateNewClient());
    }
}
