using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveManager
{
    public static void ClearSaves()
    {
        PlayerPrefs.DeleteAll();
    }

    public static int GetScore() => PlayerPrefs.GetInt("Score", 0);

    public static void SaveScore(int value)
    {
        PlayerPrefs.SetInt("Score", value);
        Save();
    }

    public static int IsSound() => PlayerPrefs.GetInt("IsSound", 1);

    public static void SaveIsSound(int value)
    {
        PlayerPrefs.SetInt("IsSound", value);
        Save();
    }

    public static int IsMusic() => PlayerPrefs.GetInt("IsMusic", 1);

    public static void SaveIsMusic(int value)
    {
        PlayerPrefs.SetInt("IsMusic", value);
        Save();
    }

    private static void Save()
    {
        PlayerPrefs.Save();
    }
}
