using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private Text scoreText;

    private int score;
    private int productCost = 10;

    private void Start()
    {
        score = SaveManager.GetScore();
        UpdateScoreText(score);
    }

    public void UpdateScore(bool isOrderAssembledCorrectly, int productsAmount)
    {
        int income = productCost * productsAmount;
        if (isOrderAssembledCorrectly) income *= 2;

        score += income;
        UpdateScoreText(score);
        SaveManager.SaveScore(score);
    }

    private void UpdateScoreText(int score)
    {
        scoreText.text = "$ " + score.ToString();
    }
}
