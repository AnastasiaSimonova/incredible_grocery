using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
	public static SoundManager Instance = null;

	public AudioSource SoundSource;
	public AudioSource MusicSource;

	[SerializeField] private AudioClip onButtonClickClip;
	[SerializeField] private AudioClip moneyClip;
	[SerializeField] private AudioClip dialogWindowAppearClip;
	[SerializeField] private AudioClip dialogWindowDisappearClip;


	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}

	private void Start()
	{
		MuteMusic(SaveManager.IsMusic() == 1 ? true : false);
		MuteSound(SaveManager.IsSound() == 1 ? true : false);

		PlayMusic();
	}

	public void MuteSound(bool value)
	{
		SoundSource.mute = !value;
	}

	public void MuteMusic(bool value)
	{
		MusicSource.mute = !value;
	}

	public void PlaySound(AudioClip clip)
	{
		SoundSource.PlayOneShot(clip);
	}

	public void PlayMusic()
	{
		//MusicSource.clip = clip;
		MusicSource.Play();
	}

	public void PlayOnButtonClickSound()
	{
		PlaySound(onButtonClickClip);
	}

	public void PlayDialogWindowAppearSound(bool appear)
	{
		var clip = appear ? dialogWindowAppearClip : dialogWindowDisappearClip;
		PlaySound(clip);
	}
}
