using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public delegate bool ToggleAction(FoodButton foodButton);

public class FoodButton : MonoBehaviour
{
    [SerializeField] private Image background;
    [SerializeField] private AudioClip onClickClip;

    public ToggleAction OnTryToggle;

    private Toggle toggle;
    private Color backgroundColor = Color.white;

    public void Reset()
    {
        backgroundColor.a = 1f;
        background.color = backgroundColor;

        toggle.onValueChanged.RemoveListener(ToggleOnValueChanged);
        toggle.isOn = false;
        toggle.onValueChanged.AddListener(ToggleOnValueChanged);
    }

    private void Start()
    {
        toggle = GetComponent<Toggle>();

        Reset();

        toggle.onValueChanged.RemoveListener(ToggleOnValueChanged);
        toggle.onValueChanged.AddListener(ToggleOnValueChanged);
    }

    public void ApplyTexture(string texturePath)
    {
        background.sprite = Resources.Load<Sprite>(texturePath);
    }

    public void Disable(bool value)
    {
        toggle.enabled = !value;
    }
    public bool SetState(bool IsSelected)
    {
        bool ToggleSucess = OnTryToggle(this);

        if (ToggleSucess)
        {
            backgroundColor.a = IsSelected ? 0.3f : 1f;
            background.color = backgroundColor;
        }
        else
        {
            toggle.onValueChanged.RemoveListener(ToggleOnValueChanged);
            toggle.isOn = false;
            toggle.onValueChanged.AddListener(ToggleOnValueChanged);
        }

        return ToggleSucess;
    }

    public void ToggleOnValueChanged(bool IsSelected)
    {
        if (SetState(IsSelected))
        {
            SoundManager.Instance.PlaySound(onClickClip);
        }
    }  
    
    public bool IsSelected()
    {
        return toggle.isOn;
    }
}
