using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FoodStorage
{
    public static List<string> productsInStorage = new List<string>()
    {
        "apple", "avocado", "bananas", "bacon",
        "beer", "onion", "berry", "cashew",
        "bone", "borscht", "chicken", "bread",
        "broccoli", "butter", "cabbage", "cake",
        "chocolate", "lollipop", "candy", "carrot"
    };
}
