using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoragePanel : MonoBehaviour
{
    [SerializeField] private GameObject foodButtonsContainer;
    [SerializeField] private GameObject foodButtonPrefab;
    [SerializeField] private Button buttonSell;

    private string productsSpritesPath = "Sprites/Products/";

    private Vector2 firstFoodObjectsPos = new Vector2(-140, 185);
    private int stepX = 90;
    private int stepY = -102;

    private Vector2 startPos = new Vector2(960, 37);
    private Vector2 finalPos = new Vector2(463, 37);
    private float animDuration = 0.5f;

    private List<FoodButton> foodButtons = new List<FoodButton>();

    private string OnStoragePanelAppearTimerCompletedEventName = "OnStoragePanelAppearTimerCompleted";

    public ToggleAction OnFoodButtonClicked;

    private void OnEnable()
    {
        EventManager.StartListening(OnStoragePanelAppearTimerCompletedEventName, OnStoragePanelAppearTimerCompleted);
    }

    private void OnDisable()
    {
        EventManager.StopListening(OnStoragePanelAppearTimerCompletedEventName, OnStoragePanelAppearTimerCompleted);
    }

    private void Start()
    {
        SetButtonSellActive(false);
        FillStorage();
    }

    public void DisableAllClickedFoodButtons(bool value)
    {
        foreach(FoodButton foodButton in foodButtons)
        {
            if (!foodButton.IsSelected())
                foodButton.Disable(value);
        }
    }

    public void SetButtonSellActive(bool value)
    {
        buttonSell.interactable = value;
    }

    // The storage panel becomes visible and moves to the game screen
    private void Appear()
    {
        StartCoroutine(AnimateMove(startPos, finalPos, animDuration));
    }

    private void Disappear()
    {
        StartCoroutine(AnimateMove(finalPos, startPos, animDuration));
    }

    private void FillStorage()
    {
        var columns = 4;
        var rows = 5;

        var productId = 0;

        for (int column = 0; column < columns; column++)
        {
            for (int row = 0; row < rows; row++)
            {
                var spritePath = productsSpritesPath + FoodStorage.productsInStorage[productId];

                var foodObject = Instantiate(foodButtonPrefab);
                foodObject.name = FoodStorage.productsInStorage[productId];
                foodObject.transform.SetParent(foodButtonsContainer.transform);
                foodObject.transform.localPosition = firstFoodObjectsPos + new Vector2(stepX * column, stepY * row);

                FoodButton foodButton = foodObject.GetComponent<FoodButton>();
                foodButton.ApplyTexture(spritePath);
                foodButton.OnTryToggle += OnFoodButtonClicked;
                foodButtons.Add(foodButton);

                productId++;
            }
        }
    }

    IEnumerator AnimateMove(Vector3 currentPos, Vector3 targetPos, float duration)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey += Time.deltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            transform.localPosition = Vector3.Lerp(currentPos, targetPos, percent);

            yield return null;
        }
    }

    public void OnStoragePanelAppearTimerCompleted()
    {
        Appear();
    }

    public void OnButtonSellClick()
    {
        SetButtonSellActive(false);
        Disappear();
        EventManager.TriggerEvent("OnProductOrderCollected");
        DisableAllClickedFoodButtons(false);
    }
}
